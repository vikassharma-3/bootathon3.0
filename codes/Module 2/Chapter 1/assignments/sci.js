var n1 = document.getElementById("n1");
var n2 = document.getElementById("n2");
var answer = document.getElementById("answer");
function add() {
    var x = +n1.value;
    var y = +n2.value;
    if (isNaN(x) || isNaN(y)) {
        alert("Wrong Input. Enter number only");
    }
    var badd = parseFloat(n1.value) + parseFloat(n2.value);
    answer.value = badd.toString();
}
function sub() {
    var x = +n1.value;
    var y = +n2.value;
    if (isNaN(x) || isNaN(y)) {
        alert("Wrong Input. Enter number only");
    }
    var bsub = parseFloat(n1.value) - parseFloat(n2.value);
    answer.value = bsub.toString();
}
function mul() {
    var x = +n1.value;
    var y = +n2.value;
    if (isNaN(x) || isNaN(y)) {
        alert("Wrong Input. Enter number only");
    }
    var bmul = parseFloat(n1.value) * parseFloat(n2.value);
    answer.value = bmul.toString();
}
function div() {
    var x = +n1.value;
    var y = +n2.value;
    if (isNaN(x) || isNaN(y)) {
        alert("Wrong Input. Enter number only");
    }
    var bdiv = parseFloat(n1.value) / parseFloat(n2.value);
    answer.value = bdiv.toString();
}
function sine() {
    var x = +n1.value;
    var y = +n2.value;
    if (isNaN(x) || isNaN(y)) {
        alert("Wrong Input. Enter number only");
    }
    let a = (180 / Math.PI) * parseFloat(n1.value);
    var bsine = Math.sin(a);
    answer.value = bsine.toString();
}
function cosine() {
    var x = +n1.value;
    var y = +n2.value;
    if (isNaN(x) || isNaN(y)) {
        alert("Wrong Input. Enter number only");
    }
    let a = (180 / Math.PI) * parseFloat(n1.value);
    var bcos = Math.cos(a);
    answer.value = bcos.toString();
}
function tan() {
    var x = +n1.value;
    var y = +n2.value;
    if (isNaN(x) || isNaN(y)) {
        alert("Wrong Input. Enter number only");
    }
    let a = (180 / Math.PI) * parseFloat(n1.value);
    var btan = Math.tan(a);
    answer.value = btan.toString();
}
function sqrt() {
    var x = +n1.value;
    var y = +n2.value;
    if (isNaN(x) || isNaN(y)) {
        alert("Wrong Input. Enter number only");
    }
    var bsqrt = Math.sqrt(parseFloat(n1.value));
    answer.value = bsqrt.toString();
}
function power() {
    var x = +n1.value;
    var y = +n2.value;
    if (isNaN(x) || isNaN(y)) {
        alert("Wrong Input. Enter number only");
    }
    var bpower = Math.pow(parseFloat(n1.value), parseFloat(n2.value));
    answer.value = bpower.toString();
}
//# sourceMappingURL=sci.js.map