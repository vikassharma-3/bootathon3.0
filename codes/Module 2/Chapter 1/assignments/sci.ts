var n1:HTMLInputElement=<HTMLInputElement>document.getElementById("n1");
var n2:HTMLInputElement=<HTMLInputElement>document.getElementById("n2");
var answer:HTMLInputElement=<HTMLInputElement>document.getElementById("answer");


function add()
{
    var x:number= +n1.value;
    var y:number= +n2.value;

    if(isNaN(x) || isNaN(y) )
{
    alert("Wrong Input. Enter number only");
}
    var badd:number=parseFloat(n1.value)+parseFloat(n2.value);
    answer.value=badd.toString();
}
function sub(){
    var x:number= +n1.value;
    var y:number= +n2.value;

    if(isNaN(x) || isNaN(y) )
{
    alert("Wrong Input. Enter number only");
}
    var bsub:number=parseFloat(n1.value)-parseFloat(n2.value);
    answer.value=bsub.toString();

}
function mul(){
    var x:number= +n1.value;
    var y:number= +n2.value;

    if(isNaN(x) || isNaN(y) )
{
    alert("Wrong Input. Enter number only");
}
    var bmul:number=parseFloat(n1.value)*parseFloat(n2.value);
    answer.value=bmul.toString();

}
function div(){
    var x:number= +n1.value;
    var y:number= +n2.value;

    if(isNaN(x) || isNaN(y) )
{
    alert("Wrong Input. Enter number only");
}
    var bdiv:number=parseFloat(n1.value)/parseFloat(n2.value);
    answer.value=bdiv.toString();

}
function sine(){
    var x:number= +n1.value;
    var y:number= +n2.value;

    if(isNaN(x) || isNaN(y) )
{
    alert("Wrong Input. Enter number only");
}
    let a:number=(180/Math.PI)*parseFloat(n1.value);
    var bsine:number=Math.sin(a);
    answer.value=bsine.toString();
}
function cosine(){
    var x:number= +n1.value;
    var y:number= +n2.value;

    if(isNaN(x) || isNaN(y) )
{
    alert("Wrong Input. Enter number only");
}
    let a:number=(180/Math.PI)*parseFloat(n1.value);
    var bcos:number=Math.cos(a);
    answer.value=bcos.toString();
}

function tan(){
    var x:number= +n1.value;
    var y:number= +n2.value;

    if(isNaN(x) || isNaN(y) )
{
    alert("Wrong Input. Enter number only");
}
    let a:number=(180/Math.PI)*parseFloat(n1.value);
    var btan:number=Math.tan(a);
    answer.value=btan.toString();
}
function sqrt(){
    var x:number= +n1.value;
    var y:number= +n2.value;

    if(isNaN(x) || isNaN(y) )
{
    alert("Wrong Input. Enter number only");
}
    var bsqrt:number=Math.sqrt(parseFloat(n1.value));
    answer.value=bsqrt.toString();
}

function power(){
    var x:number= +n1.value;
    var y:number= +n2.value;

    if(isNaN(x) || isNaN(y) )
{
    alert("Wrong Input. Enter number only");
}
    var bpower:number=Math.pow(parseFloat(n1.value), parseFloat(n2.value));
    answer.value=bpower.toString();
}
